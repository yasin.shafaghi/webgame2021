window.onload = init;

function init() {
    let cols = +(<HTMLInputElement>document.getElementById("cols")).value;
    let rows = +(<HTMLInputElement>document.getElementById("rows")).value;
    let url = (<HTMLInputElement>document.getElementById("url")).value;

    let updateButton = <HTMLButtonElement>document.getElementById('update');
    updateButton.disabled = true;

    loadImage(cols, rows, url);

    count(3, updateButton);
    let f = 2;
    for (let j = 1; j < 4; j++) {
        setTimeout(() => {
            count(f, updateButton);
            f--;
        }, 1000 * j);
    }
}

function count(i, updateButton) {
    let counterElement = document.getElementById('counter');
    counterElement.innerHTML = '<span class="countdown">' + i + '</span>';
    if (i == 0) {
        counterElement.innerHTML = '<span class="start">Start!</span>';
        updateButton.disabled = false;
    }
}

function loadImage(cols: number, rows: number, url: string) {

    removeElementsByClass('tile');

    let img = document.getElementById('wagawin');
    img.setAttribute("src", url);
    img.setAttribute("alt", "  ");
    img.setAttribute("width", "800px");

    img.onload = () => {
        setTimeout(() => shuffle(cols, rows, url), 3000);
    }
}

function shuffle(cols: number, rows: number, url: string) {
    let img = document.getElementById('wagawin');
    let p = cols * rows;
    let indexArr = generateRandom(p);
    let pairsArray = Array(p);
    let k = 0;
    for (let i = 0; i < cols; i++) {
        for (let j = 0; j < rows; j++) {
            pairsArray[indexArr[k] - 1] = {x: i, y: j};
            k++;
        }
    }
    let f = 0;
    for (let i = 0; i < cols; i++) {
        for (let j = 0; j < rows; j++) {
            // @ts-ignore
            let width = img.width / cols;
            // @ts-ignore
            let height = img.height / rows;

            let tile = document.createElement('div');
            tile.className = 'tile';
            tile.setAttribute("id", i + "" + j);

            let positionX = width * i + "px";
            let positionY = height * j + "px";

            tile.style.position = "absolute";
            tile.style.width = (width).toString() + "px";
            tile.style.height = (height).toString() + "px";
            tile.style.left = positionX;
            tile.style.top = positionY;
            tile.style.backgroundImage = "url('" + url + "')";
            tile.style.backgroundSize = "800px";
            tile.style.backgroundPosition = "-" + width * pairsArray[f].x + "px -" + height * pairsArray[f].y + "px";
            f++;

            tile.draggable = true;
            tile.ondragstart = () => drag(event);
            tile.ondragover = () => allowDrop(event);
            tile.ondrop = () => drop(event);

            tile.ontouchstart = () => handleStart(event);
            tile.ontouchmove = () => handleMove(event);
            tile.ontouchend = () => handleEnd(event);
            document.getElementById("img-container").appendChild(tile);
        }
    }
    img.setAttribute('src', null);
}

function removeElementsByClass(className) {
    const elements = document.getElementsByClassName(className);
    while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
    }
}

function generateRandom(max) {
    let arr = [];
    while (arr.length < max) {
        let r = Math.ceil(Math.random() * max);
        if (arr.indexOf(r) === -1) arr.push(r);
    }
    return arr;
}

function handleStart(ev: Event): any {
    ev.preventDefault();
    let moving = <HTMLElement>event.target;
    // store left and right to use in the touchend event
    let el = document.createElement('div');
    el.setAttribute('id', moving.id + 'start');
    el.setAttribute('x', moving.style.left);
    el.setAttribute('y', moving.style.top);
    document.getElementById('fake-div').appendChild(el);
}

function handleMove(ev: Event): any {
    let imgContainer = document.getElementById('img-container');
    let moving = <HTMLElement>ev.target;
    let touchLocation = (<TouchEvent>ev).touches[0];
    // @ts-ignore
    moving.style.left = touchLocation.pageX - imgContainer.offsetLeft - 20 + "px";
    // @ts-ignore
    moving.style.top = touchLocation.pageY - imgContainer.offsetTop - 20 + "px";
    ev.preventDefault();
}

function handleEnd(ev: Event): any {
    let moving = <HTMLElement>ev.target;
    moving.style.zIndex = "-1";
    let target = <HTMLElement>document.elementFromPoint((<TouchEvent>ev).changedTouches[0].clientX, (<TouchEvent>ev).changedTouches[0].clientY);
    let el = document.getElementById(moving.id + 'start');
    let x = el.getAttribute('x');
    let y = el.getAttribute('y');
    if (target.className !== 'tile') {
        moving.style.left = x;
        moving.style.top = y;
    } else {
        moving.style.left = target.style.left;
        moving.style.top = target.style.top;
        target.parentElement.appendChild(moving);
        target.style.left = x;
        target.style.top = y;
        target.style.zIndex = "1";
    }
    moving.style.zIndex = "1";
    el.remove();
    winAlert();
    ev.preventDefault();
}


function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    let data = {
        id: ev.target.id,
        bgPos: ev.target.style.backgroundPosition,
        left: ev.target.style.left,
        top: ev.target.style.top
    }
    let dataString = JSON.stringify(data);
    ev.dataTransfer.setData("text", dataString);
}

function drop(ev) {
    ev.preventDefault();
    let data = JSON.parse(ev.dataTransfer.getData("text"));
    let srcEl = document.getElementById(data.id);
    srcEl.style.left = data.left;
    srcEl.style.top = data.top;
    srcEl.style.backgroundPosition = ev.target.style.backgroundPosition;
    ev.target.style.backgroundPosition = data.bgPos;
    winAlert();
}

function winAlert() {
    const elements = document.getElementsByClassName('tile');
    let array = [];
    for (let el of Array.prototype.slice.call(elements)) {
        if (!isTileAtRightPlace(el)) {
            array.push(0);
        }
    }
    if (array.length == 0) {
        alert("YOU WIN, CONGRATULATIONS!");
    }
    return;
}

function isTileAtRightPlace(element: HTMLElement) {
    return ("-" + element.style.left == element.style.backgroundPositionX || element.style.left == element.style.backgroundPositionX)
        && ("-" + element.style.top == element.style.backgroundPositionY || element.style.top == element.style.backgroundPositionY)
}
