window.onload = init;
function init() {
    var cols = +document.getElementById("cols").value;
    var rows = +document.getElementById("rows").value;
    var url = document.getElementById("url").value;
    var updateButton = document.getElementById('update');
    updateButton.disabled = true;
    loadImage(cols, rows, url);
    count(3, updateButton);
    var f = 2;
    for (var j = 1; j < 4; j++) {
        setTimeout(function () {
            count(f, updateButton);
            f--;
        }, 1000 * j);
    }
}
function count(i, updateButton) {
    var counterElement = document.getElementById('counter');
    counterElement.innerHTML = '<span class="countdown">' + i + '</span>';
    if (i == 0) {
        counterElement.innerHTML = '<span class="start">Start!</span>';
        updateButton.disabled = false;
    }
}
function loadImage(cols, rows, url) {
    removeElementsByClass('tile');
    var img = document.getElementById('wagawin');
    img.setAttribute("src", url);
    img.setAttribute("alt", "  ");
    img.setAttribute("width", "800px");
    img.onload = function () {
        setTimeout(function () { return shuffle(cols, rows, url); }, 3000);
    };
}
function shuffle(cols, rows, url) {
    var img = document.getElementById('wagawin');
    var p = cols * rows;
    var indexArr = generateRandom(p);
    var pairsArray = Array(p);
    var k = 0;
    for (var i = 0; i < cols; i++) {
        for (var j = 0; j < rows; j++) {
            pairsArray[indexArr[k] - 1] = { x: i, y: j };
            k++;
        }
    }
    var f = 0;
    for (var i = 0; i < cols; i++) {
        for (var j = 0; j < rows; j++) {
            // @ts-ignore
            var width = img.width / cols;
            // @ts-ignore
            var height = img.height / rows;
            var tile = document.createElement('div');
            tile.className = 'tile';
            tile.setAttribute("id", i + "" + j);
            var positionX = width * i + "px";
            var positionY = height * j + "px";
            tile.style.position = "absolute";
            tile.style.width = (width).toString() + "px";
            tile.style.height = (height).toString() + "px";
            tile.style.left = positionX;
            tile.style.top = positionY;
            tile.style.backgroundImage = "url('" + url + "')";
            tile.style.backgroundSize = "800px";
            tile.style.backgroundPosition = "-" + width * pairsArray[f].x + "px -" + height * pairsArray[f].y + "px";
            f++;
            tile.draggable = true;
            tile.ondragstart = function () { return drag(event); };
            tile.ondragover = function () { return allowDrop(event); };
            tile.ondrop = function () { return drop(event); };
            tile.ontouchstart = function () { return handleStart(event); };
            tile.ontouchmove = function () { return handleMove(event); };
            tile.ontouchend = function () { return handleEnd(event); };
            document.getElementById("img-container").appendChild(tile);
        }
    }
    img.setAttribute('src', null);
}
function removeElementsByClass(className) {
    var elements = document.getElementsByClassName(className);
    while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
    }
}
function generateRandom(max) {
    var arr = [];
    while (arr.length < max) {
        var r = Math.ceil(Math.random() * max);
        if (arr.indexOf(r) === -1)
            arr.push(r);
    }
    return arr;
}
function handleStart(ev) {
    ev.preventDefault();
    var moving = event.target;
    // store left and right to use in the touchend event
    var el = document.createElement('div');
    el.setAttribute('id', moving.id + 'start');
    el.setAttribute('x', moving.style.left);
    el.setAttribute('y', moving.style.top);
    document.getElementById('fake-div').appendChild(el);
}
function handleMove(ev) {
    var imgContainer = document.getElementById('img-container');
    var moving = ev.target;
    var touchLocation = ev.touches[0];
    // @ts-ignore
    moving.style.left = touchLocation.pageX - imgContainer.offsetLeft - 20 + "px";
    // @ts-ignore
    moving.style.top = touchLocation.pageY - imgContainer.offsetTop - 20 + "px";
    ev.preventDefault();
}
function handleEnd(ev) {
    var moving = ev.target;
    moving.style.zIndex = "-1";
    var target = document.elementFromPoint(ev.changedTouches[0].clientX, ev.changedTouches[0].clientY);
    var el = document.getElementById(moving.id + 'start');
    var x = el.getAttribute('x');
    var y = el.getAttribute('y');
    if (target.className !== 'tile') {
        moving.style.left = x;
        moving.style.top = y;
    }
    else {
        moving.style.left = target.style.left;
        moving.style.top = target.style.top;
        target.parentElement.appendChild(moving);
        target.style.left = x;
        target.style.top = y;
        target.style.zIndex = "1";
    }
    moving.style.zIndex = "1";
    el.remove();
    winAlert();
    ev.preventDefault();
}
function allowDrop(ev) {
    ev.preventDefault();
}
function drag(ev) {
    var data = {
        id: ev.target.id,
        bgPos: ev.target.style.backgroundPosition,
        left: ev.target.style.left,
        top: ev.target.style.top
    };
    var dataString = JSON.stringify(data);
    ev.dataTransfer.setData("text", dataString);
}
function drop(ev) {
    ev.preventDefault();
    var data = JSON.parse(ev.dataTransfer.getData("text"));
    var srcEl = document.getElementById(data.id);
    srcEl.style.left = data.left;
    srcEl.style.top = data.top;
    srcEl.style.backgroundPosition = ev.target.style.backgroundPosition;
    ev.target.style.backgroundPosition = data.bgPos;
    winAlert();
}
function winAlert() {
    var elements = document.getElementsByClassName('tile');
    var array = [];
    for (var _i = 0, _a = Array.prototype.slice.call(elements); _i < _a.length; _i++) {
        var el = _a[_i];
        if (!isTileAtRightPlace(el)) {
            array.push(0);
        }
    }
    if (array.length == 0) {
        alert("YOU WIN, CONGRATULATIONS!");
    }
    return;
}
function isTileAtRightPlace(element) {
    return ("-" + element.style.left == element.style.backgroundPositionX || element.style.left == element.style.backgroundPositionX)
        && ("-" + element.style.top == element.style.backgroundPositionY || element.style.top == element.style.backgroundPositionY);
}
